#!/bin/bash

# Vérifier si le conteneur est en cours d'exécution
container_name="httpd"

if [ "$(docker ps -q -f name=$container_name)" ]; then
    # Arrêter le conteneur Apache
    echo "Arrêt du conteneur Apache..."
    docker stop $container_name
    echo "Le conteneur Apache a été arrêté avec succès."
else
    echo "Le conteneur Apache ($container_name) n'est pas en cours d'exécution."
fi