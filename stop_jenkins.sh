#!/bin/bash

JENKINS_URL="http://localhost:9090"
USERNAME="devops1"
PASSWORD="devops1"

# Obtenez le crumb pour les requêtes CSRF
CRUMB=$(curl -s "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")

# Arrêtez Jenkins en utilisant l'API
curl -X POST -H "$CRUMB" --user "$USERNAME:$PASSWORD" "$JENKINS_URL/exit"