#!/bin/bash

# Vérifier si le conteneur existe déjà
container_name="httpd"

if [ "$(docker ps -q -f name=$container_name)" ]; then
    echo "Le conteneur Apache ($container_name) est déjà en cours d'exécution."
else
    # Lancer le conteneur Apache
    echo "Lancement du conteneur Apache..."
    docker run -d -p 80:80 --name $container_name httpd:latest
    echo "Le conteneur Apache a été lancé avec succès."
fi