#!/bin/bash

# Répertoire source des fichiers à zapper
source_directory="/chemin/vers/votre/repertoire/source"

# Répertoire de destination des fichiers zippés
zip_directory="/chemin/vers/votre/repertoire/destination"

# Créer le répertoire de destination s'il n'existe pas
mkdir -p $zip_directory

# Nom du fichier zip (ajuster selon vos besoins)
zip_filename="archive_$(date +'%Y%m%d_%H%M%S').zip"

# Utiliser 7zip pour créer l'archive
7z a $zip_directory/$zip_filename $source_directory/*

# Afficher un message une fois que l'opération est terminée
echo "Fichiers zippés avec succès dans $zip_directory/$zip_filename"